/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wizut.tpsi.ogloszenia;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Danio
 */
@Service
@Transactional
public class OffersService {
    @PersistenceContext
    private EntityManager em;
    public CarManufacturer getCarManufacturer(int id) {
        return em.find(CarManufacturer.class, id);
    }
     public Offer getOffer(int id) {
        return em.find(Offer.class, id);
    }
    public CarModel getModel(int id)
    {
        return em.find(CarModel.class,id);
    }
    public List<CarManufacturer> getCarManufacturers() 
    {
    String jpql = "select cm from CarManufacturer cm order by cm.name";
    TypedQuery<CarManufacturer> query = em.createQuery(jpql, CarManufacturer.class);
    List<CarManufacturer> result = query.getResultList();
    return result;
    }
    public List<BodyStyle> getBodyStyles() 
    {
    String jpql = "select bs from BodyStyle bs order by bs.name";
    TypedQuery<BodyStyle> query = em.createQuery(jpql, BodyStyle.class);
    List<BodyStyle> result = query.getResultList();
    return result;
    }
    public List<FuelType> getFuelTypes() 
    {
    String jpql = "select ft from FuelType ft order by ft.name";
    TypedQuery<FuelType> query = em.createQuery(jpql, FuelType.class);
    List<FuelType> result = query.getResultList();
    return result;
    }
    public List<CarModel> getCarModels() 
    {
    String jpql = "select ca from CarModel ca order by ca.name";
    TypedQuery<CarModel> query = em.createQuery(jpql, CarModel.class);
    List<CarModel> result = query.getResultList();
    return result;
    }
    public List<Offer> getOffers() 
    {
    String jpql = "select ca from Offer ca order by ca.title";
    TypedQuery<Offer> query = em.createQuery(jpql, Offer.class);
    List<Offer> result = query.getResultList();
    return result;
    }
    public List<CarModel> getCarModels(int manufacturerId) 
    {
        String jpql = "select cm from CarModel cm where cm.manufacturer.id = :id order by cm.name";

        TypedQuery<CarModel> query = em.createQuery(jpql, CarModel.class);
        query.setParameter("id", manufacturerId);

        return query.getResultList();
    }
    public List<Offer> getOffersByModel(int modelId)
    {
        String jpql = "select ofr from Offer ofr where ofr.model.id = :id order by ofr.title";
        TypedQuery<Offer> query = em.createQuery(jpql, Offer.class);
        query.setParameter("id", modelId);
        return query.getResultList();
    }
    public List<Offer> getOffersByManufacturer(int manufacturerId)
    {
        String jpql = "select ofr from Offer ofr where ofr.model.manufacturer.id = :id order by ofr.title";
        TypedQuery<Offer> query = em.createQuery(jpql, Offer.class);
        query.setParameter("id", manufacturerId);
        return query.getResultList();
    }
    
    public List<Offer> getOffersByDate(int yearFrom,int yearTo)
    {
        String jpql = "select ofr from Offer ofr where ofr.year>=:yearFrom and ofr.year<=:yearTo order by ofr.title";
        TypedQuery<Offer> query = em.createQuery(jpql, Offer.class);
        query.setParameter("yearFrom", yearFrom);
        query.setParameter("yearTo", yearTo);
        return query.getResultList();
    }
    public Offer createOffer(Offer offer)
    {
        em.persist(offer);
        return offer;
    }
     public List<Offer> getOffers(OfferFilter filter)
    {
        boolean czypierwszyWarunek=true;
        String jpql ="select ofr from Offer ofr ";
        if(filter.getManufacturerId()!=null)
        {
            if(czypierwszyWarunek==true)
            {
                  jpql+="where ofr.model.manufacturer.id = :m_id ";
                  czypierwszyWarunek=false;
            }
            else
                  jpql+="and ofr.model.manufacturer.id = :m_id ";     
        }
        if(filter.getModelId()!=null)
        {
            if(czypierwszyWarunek==true)
            {
                 czypierwszyWarunek=false;
                  jpql+="where ofr.model.id = :mo_id ";
            }
            else
                  jpql+="and ofr.model.id = :mo_id ";     
        }
        if(filter.getYearFrom()!=null)
        {
            if(czypierwszyWarunek==true)
            {
                 czypierwszyWarunek=false;
                  jpql+="where ofr.year>=:yearFrom ";
            }
            else
                  jpql+="and ofr.year>=:yearFrom ";     
        }
        if(filter.getYearTo()!=null)
        {
            if(czypierwszyWarunek==true)
            {
                 czypierwszyWarunek=false;
                  jpql+="where ofr.year<=:yearTo ";
            }
            else
                  jpql+="and ofr.year<=:yearTo ";     
        }
        if(filter.getFuelId()!=null)
        {
            if(czypierwszyWarunek==true)
            {
                 czypierwszyWarunek=false;
                  jpql+="where ofr.fuelType.id = :fu_id ";
            }
            else
                  jpql+="and ofr.fuelType.id = :fu_id ";     
        }
        if(filter.getDescription()!=null)
        {
            if(czypierwszyWarunek==true)
            {
                 czypierwszyWarunek=false;
                  jpql+="where ofr.description LIKE CONCAT('%',:desc,'%') ";
            }
            else
                  jpql+="and  ofr.description LIKE CONCAT('%',:desc,'%') ";     
        }

        
        jpql+="order by ofr.title";
        TypedQuery<Offer> query = em.createQuery(jpql, Offer.class);
        if(filter.getManufacturerId()!=null)
            query.setParameter("m_id", filter.getManufacturerId());
        if(filter.getModelId()!=null)
            query.setParameter("mo_id", filter.getModelId());
        if(filter.getYearFrom()!=null)
            query.setParameter("yearFrom", filter.getYearFrom());
        if(filter.getYearTo()!=null)
            query.setParameter("yearTo", filter.getYearTo());
        if(filter.getFuelId()!=null)
            query.setParameter("fu_id", filter.getFuelId());
        if(filter.getDescription()!=null)
           query.setParameter("desc", filter.getDescription());
        if(filter.getPage()!=null)
        {
            query.setFirstResult(filter.getPage()*20-20);
        }
        else
        {
             //W 
        }
        if(filter.getSize()!=null)
        {
          //  query.setParameter("size", filter.getSize());
            query.setMaxResults(20);
        }
        return query.getResultList();
    }
    public Offer deleteOffer(Integer id)
    {
        Offer offer = em.find(Offer.class, id);
        em.remove(offer);
        return offer;
    }
    public Offer saveOffer(Offer offer) {
    return em.merge(offer);
}
}
