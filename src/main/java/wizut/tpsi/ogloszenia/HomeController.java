/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wizut.tpsi.ogloszenia;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Danio
 */
@Controller
public class HomeController {
    @Autowired
    private OffersService offersService;
    
    @RequestMapping("/home")
    public String homee(Model model) 
    {
        CarManufacturer man = offersService.getCarManufacturer(2);
        CarModel mod=offersService.getModel(3);
        model.addAttribute("CarManufacturer", man);
        model.addAttribute("CarModel", mod);
        return "index";
    }
    @GetMapping("/")
    public String home(Model model,OfferFilter offerFilter) 
    {
        List<CarManufacturer> carManufacturers = offersService.getCarManufacturers();
        List<CarModel> carModels = offersService.getCarModels();
        List<FuelType> fuelTypes=offersService.getFuelTypes();
        List<CarModel> carModelsofProducent=null;
        List<Offer> offers=null;
        double l_na_str=20.0;
        if(offerFilter.getManufacturerId()!=null) 
        {
            //Jesli wybrano producenta to ustaw listę modeli samochodów do formularza...
            carModelsofProducent=offersService.getCarModels(offerFilter.getManufacturerId());
        }
        offers=offersService.getOffers(offerFilter);
        List<Integer> str = new ArrayList<Integer>();
        
        if(offers.size()%20==0)
        {
            for(int i=1;i<=(int) Math.floor((double)offers.size()/l_na_str);i++)
            {
                str.add(i);
            }
        }
        else
        {
            for(int i=1;i<=(int) Math.floor((double)offers.size()/l_na_str)+1;i++)
            {
                str.add(i);
            }
        }
        

        model.addAttribute("carManufacturers", carManufacturers);
        model.addAttribute("fuelTypes", fuelTypes);
        model.addAttribute("carModels", carModels);
        model.addAttribute("offers", offers);
        model.addAttribute("carModelsofProducent", carModelsofProducent); 
        model.addAttribute("liczbawyn",offers.size());
        model.addAttribute("liczbastron",str);
        if(offerFilter.getPage()!=null)
            model.addAttribute("page",offerFilter.getPage());
        else
        {
            model.addAttribute("page",1);
        }
        if(offerFilter.getSize()!=null)
        {
            model.addAttribute("liczbawyn",offerFilter.getSize());
            List<Integer> str2 = new ArrayList<Integer>();
            if(offerFilter.getSize()%20==0)
            {
                for(int i=1;i<=(int)Math.floor((double)offerFilter.getSize()/20.0);i++)
                {
                    str2.add(i);
                }
            }
            else
            {
               for(int i=1;i<=(int)Math.floor((double)offerFilter.getSize()/20.0)+1;i++)
                {
                    str2.add(i);
                } 
            }
            model.addAttribute("liczbastron",str2);
            
        }
        if(offerFilter.getDescription()!=null)
        {
            model.addAttribute("description",offerFilter.getDescription());
        }
        if(offerFilter.getFuelId()!=null)
        {
            model.addAttribute("fuell",offerFilter.getFuelId());
        }
        if(offerFilter.getManufacturerId()!=null)
            model.addAttribute("manufa",offerFilter.getManufacturerId());
        if(offerFilter.getYearFrom()!=null)
            model.addAttribute("yf",offerFilter.getYearFrom());
        if(offerFilter.getYearTo()!=null)
            model.addAttribute("yt",offerFilter.getYearTo());
        if(offerFilter.getModelId()!=null)
        {
            model.addAttribute("moodel",offerFilter.getModelId());
        }
        
        
        
        return (offerFilter.getSize() != null) ? "offersList" :  ("redirect:/?size=" + (offerFilter.getSize() != null ? offerFilter.getSize().toString() : Integer.toString(offers.size())));
    }
    @GetMapping("/offer/{id}")
    public String offerDetails(Model model, @PathVariable("id") Integer id) 
    {
        Offer offer = offersService.getOffer(id);
        model.addAttribute("offer", offer);
        return "offerDetails";
    }
    @GetMapping("/newoffer")
    public String newOfferForm(Model model, Offer offer) 
    {
    List<CarModel> carModels = offersService.getCarModels();
    List<BodyStyle> bodyStyles = offersService.getBodyStyles();
    List<FuelType> fuelTypes = offersService.getFuelTypes();
    

    model.addAttribute("carModels", carModels);
    model.addAttribute("bodyStyles", bodyStyles);
    model.addAttribute("fuelTypes", fuelTypes);
    model.addAttribute("header", "Nowe ogłoszenie");
    model.addAttribute("action", "/newoffer");
        return "offerForm";
    }
    
    @PostMapping("/newoffer")
    public String saveNewOffer(Model model, @Valid Offer offer, BindingResult binding) 
    {
        if(binding.hasErrors()) 
        {
            List<CarModel> carModels = offersService.getCarModels();
            List<BodyStyle> bodyStyles = offersService.getBodyStyles();
            List<FuelType> fuelTypes = offersService.getFuelTypes();

            model.addAttribute("carModels", carModels);
            model.addAttribute("bodyStyles", bodyStyles);
            model.addAttribute("fuelTypes", fuelTypes);

            return "offerForm";
        }
        offer = offersService.createOffer(offer);

        return "redirect:/offer/" + offer.getId();
    }
    @GetMapping("/deleteoffer/{id}")
    public String deleteOffer(Model model, @PathVariable("id") Integer id)
    {
        Offer offer = offersService.deleteOffer(id);

        model.addAttribute("offer", offer);
        return "deleteOffer";
    }
    @GetMapping("/editoffer/{id}")
    public String editOffer(Model model, @PathVariable("id") Integer id) 
    {
        model.addAttribute("header", "Edycja ogłoszenia");
        model.addAttribute("action", "/editoffer/" + id);
        Offer offer = offersService.getOffer(id);
        model.addAttribute("offer", offer);
        List<CarModel> carModels = offersService.getCarModels();
        List<BodyStyle> bodyStyles = offersService.getBodyStyles();
        List<FuelType> fuelTypes = offersService.getFuelTypes();

        model.addAttribute("carModels", carModels);
        model.addAttribute("bodyStyles", bodyStyles);
        model.addAttribute("fuelTypes", fuelTypes);
        return "offerForm";
    }
    @PostMapping("/editoffer/{id}")
    public String saveEditedOffer(Model model, @PathVariable("id") Integer id, @Valid Offer offer, BindingResult binding) {
        if(binding.hasErrors()) {
            model.addAttribute("header", "Edycja ogłoszenia");
            model.addAttribute("action", "/editoffer/" + id);

            List<CarModel> carModels = offersService.getCarModels();
            List<BodyStyle> bodyStyles = offersService.getBodyStyles();
            List<FuelType> fuelTypes = offersService.getFuelTypes();

            model.addAttribute("carModels", carModels);
            model.addAttribute("bodyStyles", bodyStyles);
            model.addAttribute("fuelTypes", fuelTypes);

            return "offerForm";
        }

        offersService.saveOffer(offer);

        return "redirect:/offer/" + offer.getId();
    }

}
